login = () => {
  //login
  let username = document.getElementById('username').value;
  let password = document.getElementById('password').value;

  fetch('https://emmanuelbeja-fast-food-fast.herokuapp.com/v2/auth/login', {
      method: 'POST',
      body: JSON.stringify({
        username: username,
        password: password
      }),
      mode: 'cors',
      crossdomain: true,
      headers: {
        'Content-Type': 'application/json; charset=utf-8'
      }
    })
    .then(response => response.json())
    .then(data => {
      let userrole = data.userrole;
      window.alert(data.message);
      redirect = () => {
        if (data.code == '200') {
          location.replace("home.html");
        }
      }
      setTimeout(redirect, 1000);

      const token = data.token;
      window.sessionStorage.setItem('token', token);
      window.sessionStorage.setItem('userrole', userrole);

      res = data.message;
      return res;
    }).then(message => res).catch(error => {
      window.alert(error);
    })
}
